import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.LinkedList;
import java.util.Map;

/**
 * Modèlise le fonctionnement d'une compagnie de taxi,
 * opérant différents types de véhicules.
 * 
 * @author David J. Barnes and Michael Kölling
 * @version 2011.07.31
 */
public class TaxiCompany  
{
    // Les véhicules gérés par la compagnie
    private List<Vehicule> vehicules;
    // Les associations entre des véhicules et les passagers
    // qui sont à prendre en charge.
    private Map<Vehicule, Passager> affectations;
    public static final int tarif_de_prise_en_charge = 3;
    public static final int tarif_kilometrique = 2;
    /*
     * Pas nécessaire d'encapsuler en privé puisque constantes final
     */

    /**
     * Constructeur des objets de la classe TaxiCompany
     */
    public TaxiCompany()
    {
        vehicules = new LinkedList<Vehicule>();
        affectations = new HashMap<Vehicule, Passager>();
    }

    /**
     * Demande une course pour le passager donné.
     * @param passager Le passager demandant une prise en charge.
     * @return Si un véhicule libre est disponible.
     */
    public boolean demandeCourse(Passager passager)
    {
        Vehicule vehicule = planifieVehicule();
        if(vehicule != null) {
            affectations.put(vehicule, passager);
            vehicule.setPriseEnChargePosition(passager.getPriseEnChargePosition());
            return true;
        }
        else {
            return false;
        }
    }
    
    /**
     * Un véhicule est arrivé au point de prise en charge.
     * @param vehicle Le véhicule au point de prise en charge.
     * @throws MissingPassagerException Si il n'y a pas de passager en attente.
     */
    public void arriveAPriseEnCharge(Vehicule vehicule)
    {
        Passager passager = affectations.remove(vehicule);
        if(passager == null) {
            throw new MissingPassengerException(vehicule);
        }
        System.out.println(vehicule + " prend en charge " + passager);
        vehicule.prendEnCharge(passager);
    }
    
    /**
     * Un véhicule est arrivé à la destination demandée par un passager.
     * @param vehicule Le véhicule arrivé à la destination.
     * @param passager Le passager s'apprêtant à descendre.
     */
    public void arriveADestination(Vehicule vehicule,
                                     Passager passager)
    {
        System.out.println(vehicule + " fait descendre " + passager);
    }
    
    /**
     * @return La liste des véhicules.
     */
    public List<Vehicule> getVehicules()
    {
        return vehicules;
    }
    
    /**
     * Trouve un véhicule libre, si il y en a.
     * @return Un véhicule libre, ou null si il n'y en a pas.
     */
    private Vehicule planifieVehicule()
    {
        for (Vehicule vehicule : vehicules) {
            if (vehicule.estLibre()) {
                return vehicule;
            }
        }
        return null;
    }
    
    /**
     * Génère et retourne le prix de la course d'un passager.
     * @param passager le passager pour lequel le cout de la course doit etre calculé
     * @return prix de la course
     */
    public int getPrixDeCourse(Passager passager){
        Position departPassager = passager.getPriseEnChargePosition();
        Position arriveePassager = passager.getDestination();
        int distance = departPassager.distance(arriveePassager);
        return tarif_de_prise_en_charge + tarif_kilometrique * distance;
    }
}
