import java.util.Random;

/**
 * Modélise une position dans une ville.
 * 
 * @author David J. Barnes and Michael Kölling
 * @version 2011.07.31
 */
public class Position
{
    private int x;
    private int y;

    /**
     * Constructeur des objets de la classe Position.
     * @param x L'abscisse x. Doit être positif.
     * @param y L'ordonnée y. Doit être positif.
     * @throws IllegalArgumentException Si une coordonnée est négative.
     */
    public Position(int x, int y)
    {
        if (x < 0 || y < 0){
            throw new IllegalArgumentException("coordonnées négatives");
        } else {
            this.x = x;
            this.y = y;
        }
    }
    
    /**
     * Construit la position suivante à visiter pour
     * atteindre la destination.
     * @param destination Où nous voulons aller.
     * @return La position en ligne droite d'ici vers
     *         la destination.
     */
    public Position nextPosition(Position destination)
    {
        int destX = destination.getX();
        int destY = destination.getY();
        int deplacementX;
        int deplacementY;
        
        if (destX < this.x)
            deplacementX = -1;
        else if (destX > this.x)
            deplacementX = 1;
        else
            deplacementX = 0;
            
        if (destY < this.y)
            deplacementY = -1;
        else if (destY > this.y)
            deplacementY = 1;
        else
            deplacementY = 0;
        
        if(deplacementX != 0 || deplacementY != 0) {
            return new Position(x + deplacementX, y + deplacementY);
        }
        else {
            return destination;
        }
    }

    /**
     * Détermine le nombre de déplacements requis pour
     * aller d'ici à à la destination.
     * @param destination La destination demandée.
     * @return Le nombre de pas de déplacements.
     */
    public int distance(Position destination)
    {
        int xDist = Math.abs(destination.getX() - x);
        int yDist = Math.abs(destination.getY() - y);
        return Math.max(xDist, yDist);
    }
    
    /**
     * Définit l'égalité de contenu entre les positions.
     * @param autre Objet à comparer à cette position.
     * @return true si cette position est identique à l'autre,
     *         false autrement.
     */
    public boolean equals(Object autre)
    {
        if(autre instanceof Position) {
            Position autrePosition = (Position) autre;
            return x == autrePosition.getX() &&
                   y == autrePosition.getY();
        }
        else {
            return false;
        }
    }
    
    /**
     * @return Une représentation de la position.
     */
    public String toString()
    {
        return "position " + x + "," + y;
    }

    /**
     * Sauf pour des grilles de grande dimension, ceci devrait donner un
     * code de hachage unique pour chaque paire (x, y).
     * @return Un hashcode pour la position.
     */
    public int hashCode()
    {
        return (y << 16) + x;
    }

    /**
     * @return L'abscisse x.
     */
    public int getX()
    {
        return x;
    }

    /**
     * @return L'ordonnée y.
     */
    public int getY()
    {
        return y;
    }
}
