/**
 * Modélise un passager souhaitant aller d'une
 * position à une autre.
 * 
 * @author David J. Barnes and Michael Kölling
 * @version 2011.07.31
 */
public class Passager
{
    private Position prise_en_charge;
    private Position destination;

    /**
     * Constructeur des objets de la classe Passager
     * @param priseEnCharge La position de prise en charge, ne doit pas être null.
     * @param destination La position de destination, ne doit pas être null.
     * @throws NullPointerException Si une des positions est null.
     */
    public Passager(Position priseEnCharge, Position destination)
    {
       if (priseEnCharge == null || destination == null){
           throw new NullPointerException("position nulle");
       } else {
           if (priseEnCharge == destination){
               throw new IllegalArgumentException("positions d'arrivée et de départs identiques");
           } else {
           prise_en_charge = priseEnCharge;
           this.destination = destination;
           }
       }
    }
    
    public String toString()
    {
        return " passager voyageant de " +
               prise_en_charge + " à " + destination;
    }

    /**
     * @return La position de prise en charge.
     */
    public Position getPriseEnChargePosition()
    {
        return prise_en_charge;
    }
    
    /**
     * @return La position de destination.
     */
    public Position getDestination()
    {
        return destination;
    }
}
